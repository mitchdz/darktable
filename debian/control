Source: darktable
Section: graphics
Priority: optional
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders: David Bremner <bremner@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               intltool,
               iso-codes,
               libavif-dev,
               libcairo2-dev,
               libcolord-dev,
	       libcolord-gtk-dev,
               libcups2-dev,
               libcurl4-gnutls-dev | libcurl-dev,
               libexiv2-dev,
               libglib2.0-dev,
               libgphoto2-dev,
               libgraphicsmagick1-dev,
               libgtk-3-dev,
               libjpeg-dev,
               libjson-glib-dev,
               libjxl-dev,
               liblcms2-dev,
               liblensfun-dev,
               liblua5.4-dev,
               libopenexr-dev,
               libopenjp2-7-dev,
               libosmgpsmap-1.0-dev,
               libpng-dev,
               libportmidi-dev,
               libpugixml-dev,
               libsdl2-dev,
               librsvg2-dev,
               libsecret-1-dev,
               libsoup2.4-dev,
               libsqlite3-dev,
               libtiff-dev,
               libwebp-dev,
               xsltproc
Standards-Version: 4.1.2
Homepage: http://www.darktable.org/
Vcs-Git: https://salsa.debian.org/debian-phototools-team/darktable.git
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/darktable

Package: darktable
Architecture: any-amd64 any-arm64
Depends: ${shlibs:Depends}, ${misc:Depends}
Replaces: darktable-plugins-legacy (<< 0.9), darktable-plugins-experimental (<< 1.0~)
Breaks: darktable-plugins-legacy (<< 0.9), darktable-plugins-experimental (<< 1.0~)
Description: virtual lighttable and darkroom for photographers
 Darktable manages your digital negatives in a database and lets you view
 them  through a zoomable lighttable. it also enables you to develop raw
 images and enhance them.
 .
 It tries to fill the gap between the many excellent existing free
 raw converters and image management tools (such as ufraw or f-spot).
 The user interface is built around efficient caching of image metadata and
 mipmaps, all stored in a database. the user will always be able to interact,
 even if the full resolution image is not yet loaded.
 .
 All editing is fully non-destructive and only operates on cached image
 buffers for display. the full image is only converted during export. The
 frontend is written in gtk+/cairo, the database uses sqlite3, raw image
 loading is done using rawspeed, high-dynamic range, and standard image formats
 such as jpeg are also supported. The core operates completely on floating
 point values, so darktable can not only be used for photography but also for
 scientifically acquired images or output of renderers (high dynamic range).
